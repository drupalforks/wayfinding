<?php

namespace Drupal\wayfinding_digital_signage\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\digital_signage_framework\DeviceInterface;
use Drupal\wayfinding\Controller\Wayfinding;

/**
 * Provides the Device controller.
 */
class Device extends Wayfinding {

  /**
   * Tbd.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $digital_signage_device
   *   Tbd.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Tbd.
   */
  public function accessDevice(DeviceInterface $digital_signage_device): AccessResultInterface {
    return $digital_signage_device->isEnabled() ?
      AccessResult::allowed() :
      AccessResult::forbidden();
  }

  /**
   * Tbd.
   *
   * @param string $id
   *   Tbd.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Tbd.
   */
  public function accessExtDevice($id): AccessResultInterface {
    try {
      /** @var DeviceInterface[] $devices */
      $devices = $this->entityTypeManager->getStorage('digital_signage_device')
        ->loadByProperties([
          'extid' => $id,
        ]);
      if (empty($devices)) {
        return AccessResult::forbidden('Unknown external ID');
      }
      if (count($devices) > 1) {
        return AccessResult::forbidden('Ambiguous external ID');
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      return AccessResult::forbidden();
    }
    catch (PluginNotFoundException $e) {
      return AccessResult::forbidden();
    }
    $digital_signage_device = reset($devices);
    return $this->accessDevice($digital_signage_device);
  }

  /**
   * Tbd.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $digital_signage_device
   *   Tbd.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   Tbd.
   */
  public function requestDevice(DeviceInterface $digital_signage_device): HtmlResponse {
    $position = $digital_signage_device->get('geolocation')->getValue()[0];
    return $this->deliver(
      $digital_signage_device->get('perspective')->value,
      $position['lat'],
      $position['lng'],
      $digital_signage_device->id(),
      $digital_signage_device->extId()
    );
  }

  /**
   * Tbd.
   *
   * @param string $id
   *   Tbd.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   Tbd.
   */
  public function requestExtDevice($id): HtmlResponse {
    $devices = [];
    try {
      $devices = $this->entityTypeManager->getStorage('digital_signage_device')
        ->loadByProperties([
          'extid' => $id,
        ]);
    }
    catch (InvalidPluginDefinitionException $e) {
      // We can ignore this because it has been validated in access callback.
    }
    catch (PluginNotFoundException $e) {
      // We can ignore this because it has been validated in access callback.
    }
    $digital_signage_device = reset($devices);
    return $this->requestDevice($digital_signage_device);
  }

}
