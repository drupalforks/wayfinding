<?php

namespace Drupal\wayfinding_digital_signage;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Entity update service.
 */
class EntityUpdate {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $updateManager;

  /**
   * @var \Drupal\wayfinding_digital_signage\ContentEvent
   */
  protected $contentEvent;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   * @param \Drupal\wayfinding_digital_signage\ContentEvent $content_event
   */
  public function __construct(EntityTypeManager $entity_type_manager, EntityDefinitionUpdateManagerInterface $update_manager, ContentEvent $content_event) {
    $this->entityTypeManager = $entity_type_manager;
    $this->updateManager = $update_manager;
    $this->contentEvent = $content_event;
  }

  /**
   * @return \Drupal\Core\Field\BaseFieldDefinition
   */
  public function fieldDefinition(): BaseFieldDefinition {
    return BaseFieldDefinition::create('boolean')
      ->setLabel(t('Wayfinding link'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Show wayfinding link')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Method description.
   */
  public function updateExistingEntityTypes(): void {
    $field_definition = $this->fieldDefinition();
    /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (($definition instanceof ContentEntityTypeInterface) && $definition->id() === 'digital_signage_device') {
        $this->updateManager->installFieldStorageDefinition('wayfinding_link', $definition->id(), $definition->getProvider(), $field_definition);
      }
    }
  }

  /**
   * Method description.
   */
  public function createComputedContentForAllDevices(): void {
    /** @var \Drupal\digital_signage_framework\DeviceInterface $device */
    try {
      foreach ($this->entityTypeManager->getStorage('digital_signage_device')->loadMultiple() as $device) {
        $this->contentEvent->insert($device);
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      // TODO: Log this exception.
    }
    catch (PluginNotFoundException $e) {
      // TODO: Log this exception.
    }
  }

}
