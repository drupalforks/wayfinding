<?php

namespace Drupal\wayfinding_digital_signage\EventSubscriber;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\digital_signage_framework\DigitalSignageFrameworkEvents;
use Drupal\digital_signage_framework\Event\Rendered;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Way finding event subscriber.
 */
class Wayfinding implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * @param \Drupal\digital_signage_framework\Event\Rendered $event
   */
  public function onRendered(Rendered $event): void {
    if (!$event->getDevice()->get('wayfinding_link')->value) {
      return;
    }

    $url = Url::fromRoute('wayfinding.device.extid', ['id' => $event->getDevice()->extId()], [
      'absolute' => TRUE,
    ]);
    $link = '<div class="wayfinding-link"><a href="' . $url->toString() . '">' . $this->t('Wayfinding') . '</a></div>';
    $response = $event->getResponse();
    if ($response instanceof AjaxResponse) {
      $response->addCommand(new PrependCommand('#digital-signage-preview .popup > .content', $link));
    }
    else {
      $content = $response->getContent();
      $content .= $link;
      $response->setContent($content);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DigitalSignageFrameworkEvents::RENDERED => ['onRendered'],
    ];
  }

}
