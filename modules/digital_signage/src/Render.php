<?php

namespace Drupal\wayfinding_digital_signage;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\digital_signage_computed_content\ComputedContentInterface;
use Drupal\digital_signage_computed_content\RenderInterface;
use Drupal\digital_signage_framework\Entity\ContentSetting;
use Drupal\digital_signage_framework\Entity\Device;
use Drupal\wayfinding\Query;

/**
 * Class Render.
 *
 * @package Drupal\wayfinding_digital_signage
 */
class Render implements RenderInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\wayfinding\Query
   */
  protected $query;

  /**
   * Render constructor.
   *
   * @param \Drupal\wayfinding\Query $query
   */
  public function __construct(Query $query) {
    $this->query = $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarkup(ComputedContentInterface $entity): array {
    $settingsTarget = $entity->get('digital_signage')->getValue();
    /** @var \Drupal\digital_signage_framework\ContentSettingInterface $settings */
    if (isset($settingsTarget[0]['target_id']) &&
      ($settings = ContentSetting::load($settingsTarget[0]['target_id'])) &&
      ($device_ids = $settings->getDeviceIds())) {
      $device_id = reset($device_ids);
      /** @var \Drupal\digital_signage_framework\DeviceInterface $device */
      if ($device = Device::load($device_id)) {
        $position = $device->get('geolocation')->getValue()[0];
        return $this->query->build(
          TRUE,
          $device->get('perspective')->value,
          $position['lat'],
          $position['lng'],
          $device->id(),
          $device->extId()
        );
      }
    }
    return [
      '#markup' => $this->t('Something went wrong loading wayfinding information.'),
    ];
  }

}
