<?php

namespace Drupal\wayfinding\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wayfinding\EntityTypes;
use Drupal\wayfinding\Event\SettingsOptionEvent;
use Drupal\wayfinding\WayfindingEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Configure Way finding settings for this site.
 */
class Settings extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Field\WidgetPluginManager
   */
  protected $pluginManagerFieldWidget;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var \Drupal\wayfinding\EntityTypes
   */
  protected $entityTypesService;

  /**
   * Settings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\Core\Field\WidgetPluginManager $plugin_manager_field_widget
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   * @param \Drupal\wayfinding\EntityTypes $entity_types_service
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, WidgetPluginManager $plugin_manager_field_widget, EventDispatcherInterface $event_dispatcher, EntityTypes $entity_types_service) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->pluginManagerFieldWidget = $plugin_manager_field_widget;
    $this->eventDispatcher = $event_dispatcher;
    $this->entityTypesService = $entity_types_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.widget'),
      $container->get('event_dispatcher'),
      $container->get('wayfinding.entity_types')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wayfinding_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['wayfinding.settings'];
  }

  /**
   * @param string $widget_id
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $title
   * @param double $lat
   * @param double $lng
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getWidgetForm($widget_id, FormStateInterface $form_state, $title, $lat, $lng) {
    $widget_settings = [
      'field_definition' => BaseFieldDefinition::create('geolocation')
        ->setName('geolocation')
        ->setLabel($title)
        ->setRequired(FALSE)
        ->setCardinality(1),
      'form_mode' => 'default',
      'prepare' => TRUE,
      'configuration' => [
        'type' => $widget_id,
        'settings' => [],
        'third_party_settings' => [],
      ],
    ];
    $widget = $this->pluginManagerFieldWidget->getInstance($widget_settings);
    if (!$widget) {
      throw new PluginNotFoundException('geolocation', 'Plugin not found.');
    }
    $form['#parents'] = [];
    $element = $widget->form(FieldItemList::createInstance($widget_settings['field_definition']->getFieldStorageDefinition()), $form, $form_state)['widget'][0];
    $element['lat']['#default_value'] = $lat;
    $element['lng']['#default_value'] = $lng;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $location = $this->config('wayfinding.settings')->get('location');
    $form['#tree'] = TRUE;
    $form['location'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Geo location'),
    ];
    try {
      $form['location']['topleft'] = $this->getWidgetForm('geolocation_latlng', $form_state, $this->t('Top left'), $location['topleft']['lat'], $location['topleft']['lng']);
      $form['location']['bottomright'] = $this->getWidgetForm('geolocation_latlng', $form_state, $this->t('Bottom right'), $location['bottomright']['lat'], $location['bottomright']['lng']);
    }
    catch (PluginNotFoundException $e) {
      // TODO: handle exception.
    }
    $form['pin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URI to the pin svg'),
      '#default_value' => $this->config('wayfinding.settings')->get('pin'),
    ];
    $form['pindynamicposition'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dynamically position the pin'),
      '#default_value' => $this->config('wayfinding.settings')->get('pin dynamic position'),
    ];
    $form['enableqrcode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Support QR codes for route instructions'),
      '#default_value' => $this->config('wayfinding.settings')->get('enable qr code'),
    ];
    $form['enablepopups'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Support content popups'),
      '#default_value' => $this->config('wayfinding.settings')->get('enable popups'),
    ];
    $form['popupdestination'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination for content popups'),
      '#options' => [
        'popup' => $this->t('Popup'),
        '.legend-wrapper.before .content-container-before' => $this->t('Before early legends'),
        '.legend-wrapper.before .content-container-after' => $this->t('After early legends'),
        '.images-wrapper .content-container-before' => $this->t('Before image'),
        '.images-wrapper .content-container-after' => $this->t('After image'),
        '.legend-wrapper.after .content-container-before' => $this->t('Before late legends'),
        '.legend-wrapper.after .content-container-after' => $this->t('After late legends'),
      ],
      '#default_value' => $this->config('wayfinding.settings')->get('popup destination'),
    ];
    $form['resettimeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reset timeout in seconds'),
      '#default_value' => $this->config('wayfinding.settings')->get('reset timeout'),
    ];

    $bundles = $this->entityTypeBundleInfo->getAllBundleInfo();

    $destinations = [];
    $sources = [];
    $entity_types = [];
    foreach ($this->entityTypesService->allEnabled() as $definition) {
      if (isset($bundles[$definition->id()])) {
        $entity_types[$definition->id()] = $definition->getLabel()->render();
        foreach ($bundles[$definition->id()] as $bundle => $def) {
          if ($bundle === 'wayfinding' && $definition->id() === 'media') {
            continue;
          }
          $id = $definition->id() . ' ' . $bundle;
          $label = $definition->id() === $bundle ?
            $definition->getLabel()->render() :
            $definition->getLabel() . ' - ' . $def['label'];
          $destinations[$id] = $label;
        }
      }
    }
    foreach ($destinations as $id => $label) {
      [$entity_type, $bundle] = explode(' ', $id);
      foreach ($this->entityFieldManager->getFieldDefinitions($entity_type, $bundle) as $field) {
        if (($field->getType() === 'entity_reference' || $field->getType() === 'dynamic_entity_reference') &&
            $field->getName() !== 'digital_signage' &&
            strpos($field->getName(), 'revision_') !== 0) {
          $target_type = $field->getSetting('target_type');
          if (!isset($sources[$entity_type]) && isset($entity_types[$entity_type]) && isset($entity_types[$target_type])) {
            $sources[$entity_type] = $entity_types[$entity_type];
          }
        }
      }
    }

    $event = new SettingsOptionEvent($sources, $destinations);
    $this->eventDispatcher->dispatch(WayfindingEvents::SETTINGSOPTIONS, $event);

    $form['types'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity types'),
    ];
    $form['types']['destinations'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Destinations'),
      '#options' => $event->getDestinations(),
      '#default_value' => $this->config('wayfinding.settings')->get('types.destinations'),
      '#description' => $this->t('Destination entities will be displayed in the map if you enable the "published" checkbox in the wayfinding tab of the entity edit form.'),
    ];
    $form['types']['sources'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Sources'),
      '#options' => $event->getSources(),
      '#default_value' => $this->config('wayfinding.settings')->get('types.sources'),
      '#description' => $this->t('Source entity types are limited to those that have an entity reference to one of the available destination entity types. Select those types that you want to include in the wayfinding legend and enable the "published" checkbox in the wayfinding tab of the entity edit form for the individual entities.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $pin = $form_state->getValue('pin');
    if (!file_exists($pin)) {
      $form_state->setError($form['pin'], 'Pin file does not exist.');
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('wayfinding.settings')
      ->set('location', $form_state->getValue('location'))
      ->set('pin', $form_state->getValue('pin'))
      ->set('pin dynamic position', $form_state->getValue('pindynamicposition'))
      ->set('enable qr code', $form_state->getValue('enableqrcode'))
      ->set('enable popups', $form_state->getValue('enablepopups'))
      ->set('popup destination', $form_state->getValue('popupdestination'))
      ->set('reset timeout', $form_state->getValue('resettimeout'))
      ->set('types', $form_state->getValue('types'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
