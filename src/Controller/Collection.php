<?php

namespace Drupal\wayfinding\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\wayfinding\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Collection controller.
 */
class Collection implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Tbd.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Tbd.
   *
   * @var \Drupal\wayfinding\Query
   */
  protected $query;

  /**
   * Tbd.
   *
   * @var string
   */
  protected $redirect;

  /**
   * Tbd.
   *
   * @var array
   */
  protected $perspectives;

  /**
   * Api constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Tbd.
   * @param \Drupal\wayfinding\Query $query
   *   Tbd.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Query $query) {
    $this->entityTypeManager = $entity_type_manager;
    $this->query = $query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Collection {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('wayfinding.query')
    );
  }

  /**
   * Tbd.
   *
   * @param array $rows
   *   Tbd.
   * @param string $label
   *   Tbd.
   * @param string $type
   *   Tbd.
   * @param int $id
   *   Tbd.
   * @param string $svgid
   *   Tbd.
   */
  private function buildRow(array &$rows, $label, $type = 'user', $id = 1, $svgid = NULL): void {
    $key = implode('_', [$type, $id]);
    $rows[$key] = [
      'destination' => Link::createFromRoute($label, 'entity.' . $type . '.canonical', [$type => $id]),
    ];
    if ($svgid) {
      $rows[$key]['svgid'] = Markup::create($svgid);
    }
    else {
      foreach ($this->perspectives as $perspective) {
        if ($media = $this->query->getMedia($perspective, $type, $id)) {
          $string = $this->query->getRenderedMedia($perspective, $type, $id, TRUE);
          $url = Url::fromRoute('entity.media.edit_form', ['media' => $media->id()])
            ->toString();
          $cell = Markup::create(str_replace($url, $url . '?destination=' . $this->redirect, $string));
        }
        else {
          $cell = Link::createFromRoute('create', 'entity.media.add_form', ['media_type' => 'wayfinding'], [
            'query' => [
              'destination' => $this->redirect,
              'name' => $perspective . ': ' . $label,
              'perspective' => $perspective,
              'destination_type' => $type,
              'destination_id' => $id,
            ],
          ]);
        }
        $rows[$key]['p' . $perspective] = $cell;
      }
    }
  }

  /**
   * Tbd.
   *
   * @return array
   *   Tbd.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function request(): array {
    $this->redirect = Url::fromRoute('wayfinding.collection')->toString();
    $this->perspectives = $this->query->getPerspectives();

    $header = ['destination' => ''];
    foreach ($this->perspectives as $perspective) {
      $header['p' . $perspective] = $perspective;
    }

    $rows = [];
    $this->buildRow($rows, 'Background');
    /** @var \Drupal\views\ResultRow $row */
    foreach (views_get_view_result('wayfinding', 'wayfinding_wrapper_1') as $row) {
      /** @var \Drupal\wayfinding\Entity\Wayfinding $wayfinding */
      $wayfinding = $row->_entity;
      if ($reverse_entity = $wayfinding->getReverseEntity()) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $destination */
        $destination = $this->entityTypeManager->getStorage($reverse_entity['target_type'])->load($reverse_entity['target_id']);
        $this->buildRow($rows, $destination->label(), $destination->getEntityTypeId(), $destination->id(), $wayfinding->get('svgid')->value);
      }
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('There is no way finder content yet.'),
    ];
    return $build;
  }

}
