<?php

namespace Drupal\wayfinding\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class SettingsOptionEvent
 *
 * @package Drupal\wayfinding\Event
 */
class SettingsOptionEvent extends Event {

  /**
   * @var array
   */
  protected $sources;

  /**
   * @var array
   */
  protected $destinations;

  /**
   * SettingsOptionEvent constructor.
   *
   * @param array $sources
   * @param array $destinations
   */
  public function __construct(array $sources, array $destinations) {
    $this->setSources($sources);
    $this->setDestinations($destinations);
  }

  /**
   * @return array
   */
  public function getSources() {
    return $this->sources;
  }

  /**
   * @param array $sources
   *
   * @return $this
   */
  public function setSources(array $sources) {
    $this->sources = $sources;
    asort($this->sources);
    return $this;
  }

  /**
   * @return array
   */
  public function getDestinations() {
    return $this->destinations;
  }

  /**
   * @param array $destinations
   *
   * @return $this
   */
  public function setDestinations(array $destinations) {
    $this->destinations = $destinations;
    asort($this->destinations);
    return $this;
  }

}
