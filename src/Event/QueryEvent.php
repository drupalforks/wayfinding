<?php

namespace Drupal\wayfinding\Event;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\wayfinding\Entity\Wayfinding;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class QueryEvent
 *
 * @package Drupal\wayfinding\Event
 */
class QueryEvent extends Event {

  /**
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $destination;

  /**
   * @var ContentEntityInterface[]
   */
  protected $sources = [];

  /**
   * SettingsOptionEvent constructor.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $destination
   * @param ContentEntityInterface[] $sources
   */
  public function __construct(ContentEntityInterface $destination, array $sources) {
    $this->destination = $destination;
    $this->sources = $sources;
  }

  /**
   * @return \Drupal\Core\Entity\ContentEntityInterface
   */
  public function getDestination() {
    return $this->destination;
  }

  /**
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   */
  public function getSources() {
    return $this->sources;
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $sources
   *
   * @return $this
   */
  public function setSources(array $sources) {
    foreach ($sources as $source) {
      if ($source && $source->get('wayfinding')->getValue()) {
        /** @var Wayfinding $wayfinding */
        $wayfinding = Wayfinding::load($source->get('wayfinding')->getValue()[0]['target_id']);
        if ($wayfinding->isEnabled()) {
          $sources[] = $source;
        }
      }
    }
    return $this;
  }

}
