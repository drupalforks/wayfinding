<?php

namespace Drupal\wayfinding;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a wayfinding entity type.
 */
interface WayfindingInterface extends ContentEntityInterface {

  /**
   * Get the parent entity.
   *
   * @return array|NULL
   */
  public function getReverseEntity();

  /**
   * Sets the parent entity.
   *
   * @param ContentEntityInterface $entity
   *
   * @return \Drupal\wayfinding\WayfindingInterface
   */
  public function setReverseEntity($entity): WayfindingInterface;

  /**
   * Sets the parent entity type and bundle.
   *
   * @param string $type_and_bundle
   *
   * @return \Drupal\wayfinding\WayfindingInterface
   */
  public function setReverseEntityTypeAndBundle($type_and_bundle): WayfindingInterface;

  /**
   * Returns the wayfinding status.
   *
   * @return bool
   *   TRUE if wayfinding is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the wayfinding status.
   *
   * @param bool $status
   *   TRUE to enable wayfinding, FALSE to disable.
   *
   * @return \Drupal\wayfinding\WayfindingInterface
   *   The called wayfinding entity.
   */
  public function setStatus($status): WayfindingInterface;

  /**
   * Returns the wayfinding auto-label mode.
   *
   * @return bool
   *   TRUE if auto-label is enabled, FALSE otherwise.
   */
  public function isAutoLabel(): bool;

  /**
   * Returns the wayfinding label.
   *
   * @return string
   */
  public function getLabel(): string;

  /**
   * Sets the wayfinding label.
   *
   * @param string $label
   *
   * @return \Drupal\wayfinding\WayfindingInterface
   */
  public function setLabel($label): WayfindingInterface;

}
