<?php

namespace Drupal\wayfinding;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\wayfinding\Entity\Wayfinding;
use Drupal\wayfinding\Event\QueryEvent;
use Drupal\wayfinding\Plugin\views\style\Master;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Wayfinfing query service.
 */
class Query {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var array
   */
  protected $sources;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\Core\Database\Connection $conncetion
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   */
  public function __construct(EntityTypeManager $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, Connection $conncetion, ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher, ModuleHandlerInterface $module_handler, RendererInterface $renderer, RequestStack $request_stack) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->connection = $conncetion;
    $this->config = $config_factory->get('wayfinding.settings');
    $this->eventDispatcher = $event_dispatcher;
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
    $this->requestStack = $request_stack;
  }

  /**
   * @param double $perspective
   * @param string $type
   * @param int $id
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|NULL
   */
  public function getMedia($perspective, $type = 'user', $id = 1): ?ContentEntityInterface {
    /** @var \Drupal\Core\Entity\ContentEntityInterface[] $entities */
    try {
      $entities = $this->entityTypeManager->getStorage('media')
        ->loadByProperties([
          'field_perspective' => $perspective,
          'field_destination' => [
            'target_id' => $id,
          ],
        ]);
      foreach ($entities as $entity) {
        if ($entity->get('field_destination')->getValue()[0]['target_type'] === $type) {
          return $entity;
        }
      }
    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
    }
    return NULL;
  }

  /**
   * Build the rendered svg image.
   *
   * @param double $perspective
   * @param string $type
   * @param int $id
   * @param bool $source
   *
   * @return array|string
   */
  public function getRenderedMedia($perspective, $type = 'user', $id = 1, $source = FALSE) {
    if ($media = $this->getMedia($perspective, $type, $id)) {
      if ($source && $file = File::load($media->get('field_svg_image')->getValue()[0]['target_id'])) {
        $content = file_get_contents($file->getFileUri());
        if ($pos = strpos($content, '<svg')) {
          $content = substr($content, $pos);
        }
        return $content;
      }
      return $this->entityTypeManager->getViewBuilder($media->getEntityTypeId())
        ->view($media, 'wayfinding');
    }
    return [];
  }

  /**
   * Build the rendered pin svg image.
   *
   * @param bool $removeSvgWrapper
   *
   * @return string
   */
  public function getRenderedPin($removeSvgWrapper = FALSE): string {
    $content = file_get_contents($this->config->get('pin'));
    if ($pos = strpos($content, '<svg')) {
      $content = substr($content, $pos);
    }
    if ($removeSvgWrapper) {
      $pos = strpos($content, '>');
      $content = substr($content, $pos + 1);
      $content = str_replace('</svg>', '', $content);
    }
    return $content;
  }

  /**
   * @return array
   */
  public function getPerspectives(): array {
    $perspectives = ['0.00' => 0.00];
    if ($this->moduleHandler->moduleExists('wayfinding_digital_signage')) {
      /** @noinspection NullPointerExceptionInspection */
      $perspectives += $this->connection
        ->query('select distinct(perspective) from {digital_signage_device_field_data} where perspective is not null and perspective > 0')
        ->fetchAllKeyed(0, 0);
    }
    return $perspectives;
  }

  private function prepareSource() {
    if (!isset($this->sources)) {
      $this->sources = [];
      foreach (Master::getDestinations() as $item) {
        try {
          /** @var \Drupal\Core\Entity\ContentEntityInterface $destination */
          $destination = $this->entityTypeManager->getStorage($item['target_type'])->load($item['target_id']);
          foreach ($this->getSources($destination) as $source) {
            if (!in_array($item['target_id'], $this->sources[$source->getEntityTypeId()][$source->id()][$item['target_type']] ?? [], TRUE)) {
              $this->sources[$source->getEntityTypeId()][$source->id()][$item['target_type']][] = $item['target_id'];
            }
          }
        }
        catch (InvalidPluginDefinitionException $e) {
          // Deliberately no handling.
        }
        catch (PluginNotFoundException $e) {
          // Deliberately no handling.
        }
      }
    }
  }

  /**
   * @param array $srcid
   *
   * @return array
   */
  public function getDestinationsForId($srcid): array {
    $this->prepareSource();
    $destinations = [];
    foreach ($this->sources[$srcid['target_type']][$srcid['target_id']] ?? [] as $type => $ids) {
      foreach ($ids as $id) {
        $destinations[] = [
          'target_type' => $type,
          'target_id' => $id,
        ];
      }
    }
    return $destinations;
  }

  /**
   * Tbd.
   *
   * @param bool $inline
   *   Tbd.
   * @param float $perspective
   *   Tbd.
   * @param int $lat
   *   Tbd.
   * @param int $lng
   *   Tbd.
   * @param int $did
   *   Tbd.
   * @param string $eid
   *   Tbd.
   *
   * @return array
   *   Tbd.
   */
  public function build($inline, $perspective, $lat = 0, $lng = 0, $did = 0, $eid = 'undefined'): array {
    if (($media = $this->getMedia($perspective)) && $positions = $media->get('field_top_left')->getValue()) {
      $position = $positions[0];
    }
    else {
      $position = [
        'lat' => 0,
        'lng' => 0,
      ];
    }
    $attached = [
      'drupalSettings' => [
        'wayfinding' => [
          'widgeturl' => Url::fromRoute('wayfinding.widgets', [], [
            'absolute' => TRUE,
          ])->toString(TRUE)->getGeneratedUrl(),
          'pinDynamicPosition' => $this->config->get('pin dynamic position'),
          'resetTimeout' => $this->config->get('reset timeout') * 1000,
          'perspective' => $perspective,
          'location' => $this->config->get('location'),
          'topleft' => [
            'lat' => $position['lat'],
            'lng' => $position['lng'],
          ],
          'position' => [
            'lat' => $lat,
            'lng' => $lng,
          ],
        ],
      ],
      'library' => [
        'wayfinding/general',
      ],
    ];
    $output = [
      '#theme' => 'wayfinding',
      '#view' => views_embed_view('wayfinding', 'wayfinding_wrapper_1', $perspective, $did),
      '#blocks' => [],
      '#did' => $did,
      '#eid' => $eid,
      '#attached' => $inline ? $attached : [],
    ];
    return $inline ? $output : [
      '#markup' => $this->renderer->renderPlain($output),
      '#attached' => $attached + [
        'html_response_attachment_placeholders' => [
          'styles' => '<styles/>',
          'scripts' => '<scripts/>',
          'scripts_bottom' => '<scripts_bottom/>',
        ],
        'placeholders' => [],
      ],
    ];
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $destination
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   */
  protected function getSources($destination): array {
    $sources = [];
    $sourceTypes = $this->config->get('types.sources');

    // Get all sources referenced by the destination.
    foreach ($this->entityFieldManager->getFieldDefinitions($destination->getEntityTypeId(), $destination->bundle()) as $field) {
      if ($field->getName() !== 'digital_signage' &&
        strpos($field->getName(), 'revision_') !== 0 &&
        ($field->getType() === 'entity_reference' || $field->getType() === 'dynamic_entity_reference')) {
        $valid = FALSE;
        if ($field->getType() === 'entity_reference') {
          $valid = isset($sourceTypes[$field->getSetting('target_type')]);
        }
        elseif ($field->getType() === 'dynamic_entity_reference') {
          foreach ($field->getSetting('entity_type_ids') as $type) {
            if (isset($sourceTypes[$type])) {
              $valid = TRUE;
              break;
            }
          }
        }
        if ($valid) {
          foreach ($destination->get($field->getName())->getValue() as $item) {
            $target_type = $item['target_type'] ?? $field->getSetting('target_type');
            $this->addSource($sources, $target_type, $item['target_id']);
          }
        }
      }
    }

    // Get all sources referencing this destination.
    foreach ($sourceTypes as $sourceType) {
      foreach (array_keys($this->entityTypeBundleInfo->getBundleInfo($sourceType)) as $bundle) {
        foreach ($this->entityFieldManager->getFieldDefinitions($sourceType, $bundle) as $field) {
          if ($field->getName() !== 'digital_signage' &&
            strpos($field->getName(), 'revision_') !== 0 &&
            ($field->getType() === 'entity_reference' || $field->getType() === 'dynamic_entity_reference')) {
            $valid = FALSE;
            if ($field->getType() === 'entity_reference') {
              $valid = $field->getSetting('target_type') === $destination->getEntityTypeId();
            }
            elseif ($field->getType() === 'dynamic_entity_reference') {
              foreach ($field->getSetting('entity_type_ids') as $type) {
                if ($type === $destination->getEntityTypeId()) {
                  $valid = TRUE;
                  break;
                }
              }
            }
            if ($valid) {
              try {
                $ids = $this->entityTypeManager->getStorage($sourceType)
                  ->getQuery()
                  ->condition($field->getName(), $destination->id())
                  ->execute();
                foreach ($ids as $id) {
                  $this->addSource($sources, $sourceType, $id);
                }
              }
              catch (InvalidPluginDefinitionException $e) {
              }
              catch (PluginNotFoundException $e) {
              }
            }
          }
        }
      }
    }

    $event = new QueryEvent($destination, $sources);
    $this->eventDispatcher->dispatch(WayfindingEvents::QUERY, $event);
    $sources = array_merge($sources, $event->getSources());
    return $sources;
  }

  /**
   * @param array $sources
   * @param string $type
   * @param int $id
   */
  protected function addSource(array &$sources, $type, $id): void {
    try {
      /** @noinspection PhpUndefinedMethodInspection */
      /** @var \Drupal\wayfinding\Entity\Wayfinding $wayfinding */
      if (($source = $this->entityTypeManager->getStorage($type)->load($id)) &&
        ($value = $source->get('wayfinding')->getValue()) &&
        ($wayfinding = Wayfinding::load($value[0]['target_id'])) &&
        $wayfinding->isEnabled()) {
        $sources[] = $source;
      }
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }
  }

  public function getPopupDestination() {
    return $this->config->get('popup destination');
  }

  public function getPopupContent() {
    if (!$this->config->get('enable popups') || $this->requestStack->getCurrentRequest()->get('no-popup-content', FALSE)) {
      return '';
    }
    $this->prepareSource();
    $output = '';
    foreach ($this->sources as $entity_type => $sources) {
      foreach ($sources as $entity_id => $destinations) {
        $output .= $this->buildPopupContent($entity_type, $entity_id);
      }
    }
    return $output;
  }

  private function buildPopupContent($entityType, $entityId) {
    try {
      $entity = $this->entityTypeManager->getStorage($entityType)
        ->load($entityId);
      $output = $this->entityTypeManager->getViewBuilder($entityType)
        ->view($entity, 'wayfinding');
      $srcid = 'wayfinding-src-id-' . $entity->getEntityTypeId() . '-' . $entity->id();
      return '<div class="popup-content ' . $srcid . '">' . $this->renderer->renderPlain($output) . '</div>';
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }

  }

}
