<?php

namespace Drupal\wayfinding\Plugin\views\display;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RendererInterface;
use Drupal\views\Plugin\views\display\Attachment;
use Drupal\wayfinding\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The plugin that handles a wayfinding legend.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "wayfinding_legend",
 *   title = @Translation("Wayfinding Legend"),
 *   help = @Translation("Display the view as a wayfinding legend."),
 *   theme = "views_view",
 *   register_theme = FALSE,
 *   uses_hook_block = FALSE,
 *   contextual_links_locations = {""},
 *   admin = @Translation("Wayfinding Legend")
 * )
 */
class Legend extends Attachment {

  /**
   * @var \Drupal\wayfinding\Query
   */
  protected $query;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Master constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\wayfinding\Query $query
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Query $query, EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->query = $query;
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->config = $config_factory->get('wayfinding.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wayfinding.query'),
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $output = parent::execute();
    $this->processRows($output['#rows']);
    return $output;
  }

  /**
   * @param array $rows
   */
  private function processRows(array &$rows) {
    foreach (Element::children($rows) as $id) {
      $outerRow = &$rows[$id];
      if (!empty($outerRow['#rows'])) {
        foreach ($outerRow['#rows'] as &$row) {
          if (empty($row['#row'])) {
            $this->processRows($row);
          }
          else {
            $entity = $row['#row']->_entity->getReverseEntity();
            $srcid = 'wayfinding-src-id-' . $entity['target_type'] . '-' . $entity['target_id'];
            $destids = [];
            foreach ($this->query->getDestinationsForId($entity) as $destination) {
              $destids[] = 'wayfinding-id-' . $destination['target_type'] . '-' . $destination['target_id'];
            }
            if (empty($destids)) {
              // Assume the source is also the destination.
              $destids[] = 'wayfinding-id-' . $entity['target_type'] . '-' . $entity['target_id'];
            }
            $row['#prefix'] = '<div class="source" id="' . $srcid . '" destinations="' . implode(',', $destids) . '">';
            $row['#suffix'] = '</div>';
          }
        }
      }
    }
  }

}
