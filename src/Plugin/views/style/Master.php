<?php

namespace Drupal\wayfinding\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\wayfinding\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wayfinding plugin to render destinations and embed one or many legends.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "wayfinding_master",
 *   title = @Translation("Wayfinding master"),
 *   help = @Translation("Displays wayfinding destinations and embeds one or many legends."),
 *   theme = "views_view_wayfinding_master",
 *   display_types = {"normal"}
 * )
 */
class Master extends StylePluginBase {

  protected $usesGrouping = FALSE;

  /**
   * @var \Drupal\wayfinding\Query
   */
  protected $query;

  protected static $destinations = [];

  /**
   * Master constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\wayfinding\Query $query
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Query $query) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->query = $query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wayfinding.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($result) {
    /** @var \Drupal\views\ResultRow $row */
    foreach ($result as $row) {
      /** @var \Drupal\wayfinding\Entity\Wayfinding $wayfinding */
      $wayfinding = $row->_entity;
      $entity = $wayfinding->getReverseEntity();
      self::$destinations[] = $entity;
      $row->wayfinding_id = 'wayfinding-id-' . $entity['target_type'] . '-' . $entity['target_id'];
      $row->img = $this->query->getRenderedMedia(0, $entity['target_type'], $entity['target_id']);
      $row->svgid = $wayfinding->get('svgid')->value ?? [];
      $row->geolocation = $wayfinding->get('geolocation')->getValue()[0];
    }
  }

  /**
   * @return array
   */
  public static function getDestinations() {
    return self::$destinations;
  }

}
