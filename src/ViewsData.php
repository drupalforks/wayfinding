<?php

namespace Drupal\wayfinding;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the wayfinding entity type.
 */
class ViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['wayfinding']['destinations'] = [
      'title' => $this->t('Wayfinding destinations'),
      'help' => $this->t('Filters out wayfinding destination entities.'),
      'filter' => [
        'field' => 'parent_entity_type_bundle',
        'id' => 'wayfinding_destinations',
        'label' => $this->t('Wayfinding destination entities'),
      ],
    ];
    $data['wayfinding']['sources'] = [
      'title' => $this->t('Wayfinding sources'),
      'help' => $this->t('Filters out wayfinding source entities.'),
      'filter' => [
        'field' => 'parent_entity__target_type',
        'id' => 'wayfinding_sources',
        'label' => $this->t('Wayfinding source entities'),
      ],
    ];

    return $data;
  }

}
